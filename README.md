# Bookmarks


## Datasets


- [Data Lad](http://www.datalad.org/)

> DataLad aims to provide access to scientific data available from various sources (e.g. lab or consortium web-sites such as Human connectome; data sharing portals such as OpenFMRI and CRCNS) through a single convenient interface and integrated with your software package managers (such as APT in Debian). Although initially targeting neuroimaging and neuroscience data in general, it will not be limited by the domain and we would welcome a wide range of contributions.

## A.I.


- [awesome-rl](https://github.com/aikorea/awesome-rl)

> A curated list of resources dedicated to reinforcement learning.

## Misc


- [awesome-movies](https://gitlab.com/suriyadeepan/awesome-movies)

> List of recommended movies by members of Maximal entropy learning